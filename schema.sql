CREATE TABLE restaurant (
id int(10) unsigned NOT NULL AUTO_INCREMENT,
name char(20) COLLATE utf8_general_ci NOT NULL,
address char(20) COLLATE utf8_general_ci NOT NULL,
phone char(12) COLLATE utf8_general_ci DEFAULT NOT NULL,
PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE dish (
id int(10) unsigned NOT NULL AUTO_INCREMENT,
name char(20) COLLATE utf8_general_ci NOT NULL,
price NUMERIC(10, 2) DEFAULT '0.00',
category enum('drink', 'main dish', 'salads', 'appetizer', 'desert', 'alcohol');
amount DECIMAL(10, 1) DEFAULT '0.0',
PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci; 

CREATE TABLE employee (
id int(10) unsigned NOT NULL AUTO_INCREMENT,
name char(20) COLLATE utf8_general_ci NOT NULL,
surname char(20) COLLATE utf8_general_ci NOT NULL,
birthday DATE,
gender enum('male', 'female'),
position enum('chief', 'cook', 'dish washer', 'cleaner', 'waiter', 'manager')
education CHAR(255),
salary NUMERIC(10, 2) DEFAULT '0.00',
restaurant_id int(10) NOT NULL, 
PRIMARY KEY (id),

FOREIGN KEY (restaurant_id)
    REFERENCES restaurant(id)
    ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci; 

CREATE TABLE order (
id int(10) unsigned NOT NULL AUTO_INCREMENT,
table_number int(3) NOT NULL,
total_price DECIMAL(10, 2) DEFAULT '0.00',
employee_id int(10) NOT NULL,
PRIMARY KEY (id),

FOREIGN KEY (employee_id)
    REFERENCES employee(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci; 

CREATE TABLE order_dish (
    id int(10) unsigned NOT NULL AUTO_INCREMENT,
    order_id int(10) NOT NULL,
    dish_id int(10) NOT NULL,
    PRIMARY KEY (id),

    FOREIGN KEY (order_id)
        REFERENCES order(id)
        ON UPDATE CASCADE ON DELETE CASCADE,

    FOREIGN KEY (dish_id)
        REFERENCES dish(id)
        ON UPDATE CASCADE ON DELETE CASCADE,
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE dish_restaurant (
    id int(10) unsigned NOT NULL AUTO_INCREMENT,
    restaurant_id int(10) NOT NULL,
    dish_id int(10) NOT NULL,
    PRIMARY KEY (id),

    FOREIGN KEY (restaurant_id)
        REFERENCES restaurant(id)
        ON UPDATE CASCADE ON DELETE CASCADE,

    FOREIGN KEY (dish_id)
        REFERENCES dish(id)
        ON UPDATE CASCADE ON DELETE CASCADE,
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
